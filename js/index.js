window.icons = {
	'file-code': 'xml|html?|haml|php|aspx?|js|(le|sa|s?c)cs|bat|cmd|vbs|java|smali|py',
	'file-zip': 'zip|rar|gz|7z',
	'android': 'apk|jar|dex',
	'apple': 'ipa|dmg',
	'windows': 'iso|esd|exe|msi',
	'file-text': 'plain|log|msg|odt|page|te?xt|ext|md|ini|conf|svg',
	'file-music': 'aac|mid|wma|ogg|m4a|mp3|ape|flac|wav|d[sd]f',
	'file-image': 'bmp|jpe?g|png|ico|[tg]if|pc[cx]|tga|exif|fpx|psd|cdr|dxf|ufo|eps|ai|raw|wmf|webp',
	'file-play': '3gp|avi|mkv|mp4|flv|rmvb|mov|wmv',
	'file-word': 'docx?',
	'file-excel': 'xl(s[xm]?|t)',
	'file-pdf': 'pdf',
	'file-ppt': 'pp[st]x?|ppam?|potx?',
	'file-font': 'eot|otf|font?|tt[cf]|woff',
	'database': 'db|sql'
};

function getIcon (ext) {
	var re = /^(aac|ai|bmp|cs|css|csv|doc|docx|exe|gif|heic|html|java|jpg|js|json|jsx|key|m4p|md|mdx|mov|mp3|mp4|otf|pdf|php|png|ppt|pptx|psd|py|raw|rb|sass|scss|sh|sql|svg|tiff|tsx|ttf|txt|wav|woff|xls|xlsx|xml|yml)/i;
	if (re.test(ext)) return 'bi-filetype-' + ext.toLowerCase();

	for (var icon in icons) {
		re = new RegExp('^' + icons[icon] + '$', 'i');
		if (re.test(ext)) return 'bi-' + icon;
	}

	return 'bi-file';
}

function parseHash () {
	window.conf = {
		id: '',
		pwd: '',
		type: '',
		admin: false
	};
	var re = location.hash.match(/^#([1-9]\d*|[bi][0-9a-zA-Z]+)(\?(.+))?/);

	if (re) {
		window.conf.id = re[1];
		var first = re[1].substring(0, 1).toLowerCase();
		if (first === 'b' || first === 'i') window.conf.type = first;

		if (isEmpty(re[3])) return;

		var query = re[3].split('&');
		for (var i = 0; i < query.length; i++) {
			var a = query[i].split('=');
			if (window.conf.hasOwnProperty(a[0])) window.conf[a[0]] = a[1];
		}
	}
}

function buildHash (id, pwd) {
	var hash = isEmpty(id) ? '' : id;
	if (!isEmpty(pwd)) hash += '?pwd=' + pwd;
	return hash;
}

function buildUrl (uri, params) {
	if (varType(params) === 'Object' && params.length > 0) {
		uri += '?';
		for (var key in params) {
			uri += encodeURIComponent(key) + '=' + encodeURIComponent(params[key]) + '&';
		}
		uri = uri.slice(0, -1);
	}

	return location.href.replace(/[#?].*$/g, '') + uri;
}

layui.config({
	base: "js/lay-module/",
	version: true
}).extend({
	lianyi: 'lianyi'
}).use(['jquery', 'layer', 'table', 'form', 'upload', 'element', 'dropdown', 'lianyi'], function () {
	var $        = layui.jquery,
		layer    = layui.layer,
		table    = layui.table,
		form     = layui.form,
		upload   = layui.upload,
		element  = layui.element,
		dropdown = layui.dropdown,
		lianyi   = layui.lianyi;

	var tableIns;
	window.cut_file = {};

	var parseLayer = function (msg) {
		return layer.open({
			title: '涟漪云 <div class="update-time"><b class="layui-font-red">2023-08-11</b></div>',
			type: 1,
			closeBtn: false,
			shadeClose: true,
			content: $('#lanzou'),
			skin: 'input-id',
			btn: ['确定', '取消'],
			id: 'parseLayer',
			success: function (elem, index) {
				if (msg) lianyi.fail(msg);
				form.val('lanzou', {id: conf.id, pwd: conf.pwd});

				var that = this;
				elem.find('input[name]').on('input propertychange', function () {
					var val = $(this).val();
					switch ($(this).attr('name')) {
						case 'id':
							if (/[^a-zA-Z0-9]/.test(val)) $(this).val(val.replace(/[^a-zA-Z0-9]/g, ''));
							elem.find('.layui-layer-btn0').text(/^[bi]/i.test($(this).val()) ? '解析' : '确定');
							break;
						case 'pwd':
							if (/^\s+|\s+$/.test(val)) $(this).val(val.replace(/^\s+|\s+$/g, ''));
							break;
					}
					$(this).prev().children('i').css('color', $(this).val() === '' ? '#d2d2d2' : '#1e9fff');
				}).keydown(function (e) {
					if (e.keyCode === 13)
						that.yes(index, elem);
				}).trigger('input');
			},
			yes: function (index, elem) {
				var field = form.val('lanzou');

				window.conf.id = '';
				window.conf.pwd = '';
				window.conf.type = '';

				var re = field.id.match(/^([1-9]\d*|[bi][0-9a-zA-Z]+)$/);
				if (!isEmpty(re)) {
					window.conf.id = field.id;
					window.conf.pwd = field.pwd;
					var first = field.id.substring(0, 1).toLowerCase();
					if (first === 'b' || first === 'i') window.conf.type = first;
				}

				layer.close(index);
				location.hash = buildHash(field.id, field.pwd);
			}
		});
	}

	var layerLogin = function () {
		if (!conf.admin) {
			layer.open({
				title: '管理员登录',
				type: 1,
				shadeClose: true,
				content:
					'<div class="layui-form" id="login" lay-filter="login" style="padding: 20px">' +
					'	<div class="layui-input-wrap">' +
					'		<div class="layui-input-prefix">' +
					'			<i class="layui-icon layui-icon-username"></i>' +
					'		</div>' +
					'		<input type="text" name="pass" lay-verify="required" tips2 placeholder="管理员密码" lay-reqtext="请输入密码" autocomplete="off" maxlength="32" class="layui-input">' +
					'	</div>' +
					'	<div class="layui-input-wrap layui-code-wrap m-t-15">' +
					'		<div class="layui-input-prefix">' +
					'			<i class="layui-icon layui-icon-vercode"></i>' +
					'		</div>' +
					'		<input type="text" name="code" lay-verify="required" tips2 placeholder="图形验证码" lay-reqtext="请输入图形验证码" autocomplete="off" maxlength="4" class="layui-input">' +
					'		<div class="layui-input-suffix layui-input-split">' +
					'			<img src="api.php?c=verify" alt="图形验证码" width="100%" height="100%">' +
					'		</div>' +
					'	</div>' +
					'</div>'
				,
				btn: ['登录', '取消'],
				success: function (elem, index) {
					elem.find('.layui-code-wrap > .layui-input-suffix > img').on('click', function () {
						$(this).attr('src', 'api.php?c=verify&t=' + Math.random());
					});
					var that = this;
					elem.find('.layui-input-wrap input[name]').on('input propertychange', function () {
						var val = $(this).val();
						switch ($(this).attr('name')) {
							case 'pass':
								if (/\s+/.test(val)) $(this).val(val.replace(/\s+/g, ''));
								break;
							case 'code':
								if (/[^0-9a-zA-Z]/.test(val)) $(this).val(val.replace(/[^0-9a-zA-Z]/g, ''));
								break;
						}
						$(this).prev().children('i').css('color', $(this).val() === '' ? '#d2d2d2' : '#1e9fff');
					}).keydown(function (e) {
						if (e.keyCode === 13)
							that.yes(index, elem);
					});
				},
				yes: function (index, elem) {
					var btn0 = $(elem).find('.layui-layer-btn0');
					if (btn0.hasClass('layui-disabled')) return

					var field = form.val('login');
					if (field.pass === '') return lianyi.fail('管理员密码不能为空');
					if (field.code === '') return lianyi.fail('图形验证码不能为空');

					btn0.addClass('layui-disabled').prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
					lianyi.request({
						url: 'api.php?c=login',
						data: field,
						success: function (res) {
							layer.close(index);
							window.conf.admin = true;
						},
						complete: function (res) {
							btn0.removeClass('layui-disabled').children('.layui-icon.layui-icon-loading').remove();
							if (res.code >= 0)
								$(elem).find('.layui-code-wrap > .layui-input-suffix > img').click();
						}
					});
				}
			});
			return false;
		}

		return true;
	}

	var refresh = function (page) {
		layer.closeAll()

		var where = {id: conf.id, pwd: conf.pwd};

		if (table.cache.hasOwnProperty('list'))
			return table.reload('list', {
				where: where,
				page: {
					groups: 1,
					first: '首页',
					last: false,
					layout: ['prev', 'page', 'next', 'skip'],
					curr: page || 1
				},
				limit: conf.type === '' ? 18 : 50
			});

		tableIns = table.render({
			elem: '#list',
			url: 'api.php?c=list',
			method: 'POST',
			where: where,
			toolbar: '#tpl_toolbar',
			defaultToolbar: ['filter'],
			height: 'full-100',
			cols: [[
				{type: 'checkbox', title: '全选', id: 'checkAll', width: 48, align: 'center', fixed: 'left'},
				{type: 'numbers', title: '序号', width: 60, align: 'center', fixed: 'left'},
				{
					field: 'name', title: '名称', minWidth: 260, event: 'name', templet: function (d) {
						var icon = d.isdir ? 'bi-folder' : getIcon(d.ext);

						return '<i class="bi {0} layui-font-20" style="margin-right: 5px;float: left;"></i>{1}'.format(icon, d.name);
					}, sort: true
				},
				{field: 'size', title: '大小', width: 82, align: 'right', sort: true},
				{field: 'downs', title: '下载', width: 82, align: 'center', sort: true},
				{field: 'time', title: '时间', width: 102, align: 'center', sort: true},
				{title: '操作', width: 78, fixed: 'right', align: 'center', templet: '#tpl_action'}
			]],
			page: {
				groups: 1,
				first: '首页',
				last: false,
				layout: ['prev', 'page', 'next', 'skip']
			},
			limit: conf.type === '' ? 18 : 50,
			parseData: function (res) {
				tableIns.config.page.curr = this.page.curr;
				window.conf.admin = res.admin;
			},
			done: function (res) {
				cut_files(true);
				if (res.path)
					$('#nav').html(function () {
						var navs = ['<a href="#{0}">{1}</a>'.format(buildHash(), '根目录')]
						$.each(res.path, function (id, name) {
							navs.push('<a href="#{0}">{1}</a>'.format(buildHash(id), name));
						});
						return navs.join('<span lay-separator="">/</span>')
					});
				if (res.hasOwnProperty('desc')) $('.folder-desc').html(res.desc || '');

				if (res.code < 0) {
					setTimeout(function () {
						parseLayer(res.msg);
					}, 200);
				}
			}
		});
	}

	table.on('toolbar(list)', function (obj) {
		var event = obj.event,
			d     = obj.data,
			checkStatus, checkeds;
		switch (event) {
			case 'refresh':
				refresh();
				break;
			case 'change':
				parseLayer();
				break;
			case 'folder':
				if (!layerLogin()) return;

				layer.open({
					title: '新建文件夹',
					type: 1,
					shadeClose: true,
					content:
						'<div class="layui-form" lay-filter="folder" style="padding: 20px">' +
						'	<div class="layui-input-wrap">' +
						'		<div class="layui-input-prefix">' +
						'			<i class="layui-icon layui-icon-file"></i>' +
						'		</div>' +
						'		<input type="text" name="name" lay-verify="required" tips2 placeholder="文件夹名称" lay-reqtext="请输文件夹名称" autocomplete="off" maxlength="100" class="layui-input">' +
						'	</div>' +
						'	<div class="layui-input-wrap m-t-15">' +
						'		<div class="layui-input-prefix">' +
						'			<i class="layui-icon layui-icon-password"></i>' +
						'		</div>' +
						'		<input type="text" name="pwd" lay-verify="" tips2 placeholder="文件夹密码 (选填)" autocomplete="off" maxlength="12" class="layui-input">' +
						'	</div>' +
						'	<div class="layui-form-item m-t-15">' +
						'		<textarea name="desc" tips2 placeholder="文件夹描述 (选填)" maxlength="170" class="layui-textarea"></textarea>' +
						'	</div>' +
						'</div>'
					,
					btn: ['创建', '取消'],
					success: function (elem, index) {
						var that = this;
						elem.find('.layui-input-wrap input[name]').on('input propertychange', function () {
							var val = $(this).val();
							switch ($(this).attr('name')) {
								case 'name':
									if (/\s/.test(val)) $(this).val(val.replace(/\s+/g, ''));
									break;
								case 'pwd':
									if (/[^\w]/.test(val)) $(this).val(val.replace(/[^\w]+/g, ''));
									break;
							}
							$(this).prev().children('i').css('color', $(this).val() === '' ? '#d2d2d2' : '#1e9fff');
						}).keydown(function (e) {
							if (e.keyCode === 13)
								that.yes(index, elem);
						});
					},
					yes: function (index, elem) {
						var btn0 = elem.find('.layui-layer-btn0');
						if (btn0.hasClass('layui-disabled')) return;

						var field = form.val('folder');
						if (field.name === '') return lianyi.fail('文件夹名称不能为空！');

						btn0.addClass('layui-disabled').prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
						lianyi.request({
							url: 'api.php?c=folder',
							data: $.extend({id: conf.id}, field),
							msg: '新建文件夹...',
							success: function (res) {
								layer.close(index);
							},
							complete: function (res) {
								btn0.removeClass('layui-disabled').children('.layui-icon.layui-icon-loading').remove();
							}
						});
					}
				});
				break;
			case 'cut':
				checkStatus = table.checkStatus('list');
				checkeds = checkStatus.data;

				cut_files(checkeds);
				table.setRowChecked('list', {
					index: 'all',
					checked: false
				});
				$('.toolbar-admin .copy-link, .toolbar-admin [lay-event="cut"], .toolbar-admin [lay-event="delete"]')
					.addClass('layui-hide');
				break;
			case 'paste':
				if (!layerLogin()) return;

				layer.confirm(Object.values(cut_file).join('<hr>'), {
					title: '确认移动以下文件到此目录',
					shadeClose: true,
					btn: ['移动', '清空', '取消'],
					btn1: function (index) {
						lianyi.request({
							url: 'api.php?c=move',
							data: {id: conf.id, file_id: Object.keys(cut_file)},
							msg: '移动文件...',
							success: function (res) {
								layer.close(index);
							},
							complete: function (res) {
								cut_files();
							}
						});
						return false;
					},
					btn2: function (index) {
						cut_files();
					}
				});
				break;
			case 'delete':
				if (!layerLogin()) return;

				checkStatus = table.checkStatus('list');
				checkeds = checkStatus.data;

				var folder = {}, file = {};
				$.each(checkeds, function (i, v) {
					if (v.isdir) folder[v.id] = v.name;
					else file[v.id] = v.name;
				});

				layer.confirm(Object.values(folder).concat(Object.values(file)).join('<hr>'), {
					title: '确认删除以下文件（夹）',
					shadeClose: true,
					btn: ['删除', '取消'],
					btn1: function (index) {
						lianyi.request({
							url: 'api.php?c=delete',
							data: {id: conf.id, file_id: Object.keys(file), folder_id: Object.keys(folder)},
							msg: '删除文件（夹）...',
							success: function (res) {
								layer.close(index);
							}
						});
						return false;
					}
				});
				break;
		}
	});

	table.on('checkbox(list)', function (obj) {
		var checkStatus = table.checkStatus('list'),
			checkeds    = checkStatus.data;

		var file = {};
		$.each(checkeds, function (i, v) {
			if (!v.isdir) file[v.id] = v.name;
		});
		var len_checked      = checkeds.length,
			len_checked_file = Object.keys(file).length,
			len_cut          = Object.keys(window.cut_file).length;

		$('.toolbar-admin .copy-link')[len_checked_file > 0 ? 'removeClass' : 'addClass']('layui-hide');
		$('.toolbar-admin [lay-event="cut"]')[len_checked_file > 0 ? 'removeClass' : 'addClass']('layui-hide');
		$('.toolbar-admin [lay-event="paste"]')[len_cut > 0 ? 'removeClass' : 'addClass']('layui-hide');
		$('.toolbar-admin [lay-event="delete"]')[len_checked > 0 ? 'removeClass' : 'addClass']('layui-hide');
	});

	table.on('tool(list)', function (obj) {
		var d     = obj.data,
			event = obj.event;

		switch (event) {
			case 'name':
				if (d.isdir)
					location.hash = d.id;
				else
					location.href = d.id;
				break;
			case 'action':
				var disabled = !conf.admin || conf.type !== '';
				dropdown.render({
					elem: this, //触发事件的 DOM 对象
					show: true,
					// trigger: 'hover',
					className: 'dropdown-action',
					data: d.isdir ? [
						{
							id: 3,
							title: '设置密码',
							templet: '<i class="bi bi-file-lock layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 4,
							title: '重命名&描述',
							templet: '<i class="bi bi-files-alt layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 9,
							title: '删除',
							templet: '<i class="bi bi-trash layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						}
					] : [
						{id: 1, title: '直链1', templet: '<i class="bi bi-link layui-icon-left"></i> {{=d.title}}'},
						{
							id: 2,
							title: '直链2',
							templet: '<i class="bi bi-link-45deg layui-icon-left"></i> {{=d.title}}'
						},
						{type: '-'},
						{
							id: 3,
							title: '设置密码',
							templet: '<i class="bi bi-file-lock layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 5,
							title: '文件描述',
							templet: '<i class="bi bi-files-alt layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 6,
							title: '剪贴',
							templet: '<i class="bi bi-scissors layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 9,
							title: '删除',
							templet: '<i class="bi bi-trash layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						}
					],
					ready: function (elemPanel, elem) {
						if (d.isdir) return;

						var lis = $(elemPanel).find('.layui-dropdown-menu > li');
						lis.eq(0).unbind('click').attr('copy-text', buildUrl(d.id + '/' + d.name));
						lis.eq(1).unbind('click').attr('copy-text', buildUrl(d.id + (isEmpty(d.ext) ? '' : '.' + d.ext)));
					},
					click: function (menudata) {
						switch (menudata.id) {
							case 3:
								if (!layerLogin()) return;
								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: d.isdir ? 1 : 0},
									msg: '获取数据...',
									success: function (res) {
										layer.prompt(
											{
												title: menudata.title,
												formType: 0,
												shadeClose: true,
												allowBlank: true,
												value: '',
												btn: ['修改', '关闭'],
												success: function (elem) {
													elem.find('.layui-layer-content > .layui-layer-input')
														.attr('placeholder', '请输入新密码')
														.val(res.data.pwd);
												}
											},
											function (value, index, elem) {
												var btn0 = $(elem).find('.layui-layer-btn0');
												if (btn0.hasClass('layui-disabled')) return;

												btn0.addClass('layui-disabled').prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
												lianyi.request({
													url: 'api.php?c=pwd',
													data: {id: d.id, pwd: value, isdir: d.isdir ? 1 : 0},
													msg: '修改密码...',
													success: function (res) {
														layer.close(index);
													},
													complete: function (res) {
														btn0.removeClass('layui-disabled').children('.layui-icon.layui-icon-loading').remove();
													}
												});
											}
										);
									}
								});
								break;
							case 4:
								if (!layerLogin()) return;

								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: 1},
									msg: '获取数据...',
									success: function (res) {
										layer.open({
											title: menudata.title,
											type: 1,
											shadeClose: true,
											content:
												'<div class="layui-form" lay-filter="rename" style="padding: 20px">' +
												'	<div class="layui-input-wrap">' +
												'		<div class="layui-input-prefix">' +
												'			<i class="layui-icon layui-icon-file"></i>' +
												'		</div>' +
												'		<input type="text" name="name" lay-verify="required" tips2 placeholder="文件夹名称" lay-reqtext="请输文件夹名称" autocomplete="off" maxlength="100" class="layui-input">' +
												'	</div>' +
												'	<div class="layui-form-item m-t-15">' +
												'		<textarea name="desc" tips2 placeholder="文件夹描述 (选填)" maxlength="170" class="layui-textarea"></textarea>' +
												'	</div>' +
												'</div>'
											,
											btn: ['修改', '取消'],
											success: function (elem, index) {
												var that = this;
												form.val('rename', {name: res.data.name, desc: res.data.des});
												elem.find('.layui-input-wrap input[name]').on('input propertychange', function () {
													var val = $(this).val();
													switch ($(this).attr('name')) {
														case 'pass':
															if (/\s+/.test(val)) $(this).val(val.replace(/\s+/g, ''));
															break;
														case 'code':
															if (/[^0-9a-zA-Z]/.test(val)) $(this).val(val.replace(/[^0-9a-zA-Z]/g, ''));
															break;
													}
													$(this).prev().children('i').css('color', $(this).val() === '' ? '#d2d2d2' : '#1e9fff');
												}).keydown(function (e) {
													if (e.keyCode === 13)
														that.yes(index, elem);
												}).trigger('input');
											},
											yes: function (index, elem) {
												var btn0 = elem.find('.layui-layer-btn0');
												if (btn0.hasClass('layui-disabled')) return;

												var field = form.val('rename');
												if (field.name === '') return lianyi.fail('名称不能为空');

												btn0.addClass('layui-disabled').prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
												lianyi.request({
													url: 'api.php?c=rename',
													data: $.extend({id: d.id}, field),
													success: function (res) {
														obj.update(field);
														layer.close(index);
													},
													complete: function (res) {
														btn0.removeClass('layui-disabled').children('.layui-icon.layui-icon-loading').remove();
													}
												});
											}
										});
									}
								});
								break;
							case 5:
								if (!layerLogin()) return;
								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: 0},
									msg: '获取数据...',
									success: function (res) {
										layer.prompt(
											{
												title: menudata.title,
												formType: 2,
												shadeClose: true,
												allowBlank: true,
												value: res.data.des,
												btn: ['修改', '关闭'],
												success: function (elem) {
													$(elem).find('.layui-layer-content > .layui-layer-input')
														.attr('placeholder', '请输入文件描述');
												}
											},
											function (value, index, elem) {
												var btn0 = $(elem).find('.layui-layer-btn0');
												if (btn0.hasClass('layui-disabled')) return;

												btn0.addClass('layui-disabled').prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
												lianyi.request({
													url: 'api.php?c=desc',
													data: {id: d.id, desc: value},
													msg: '修改描述...',
													success: function (res) {
														layer.close(index);
													},
													complete: function (res) {
														btn0.removeClass('layui-disabled').children('.layui-icon.layui-icon-loading').remove();
													}
												});
											}
										);
									}
								});
								break;
							case 6:
								var file = {};
								file[d.id] = d.name;
								cut_files(file);
								break;
							case 9:
								if (!layerLogin()) return;

								var data = {id: conf.id};
								data[d.isdir ? 'folder_id' : 'file_id'] = d.id;
								layer.confirm('确认删除文件{0}：<b class="layui-font-red">{1}</b> ？'.format(d.isdir ? '夹' : '', d.name), {
									title: '删除文件' + (d.isdir ? '夹' : ''),
									shadeClose: true,
									btn: ['删除', '取消'],
									btn1: function (index) {
										lianyi.request({
											url: 'api.php?c=delete',
											data: data,
											msg: '正在删除...',
											success: function (res) {
												layer.close(index);
											}
										});
										return false;
									}
								});
								break;
						}
					},
					align: 'right', //右对齐弹出（v2.6.8 新增）
					style: 'box-shadow: 1px 1px 10px rgb(0 0 0 / 12%);' //设置额外样式
				});
				break;
		}
	});

	function cut_files (id) {
		if (typeof (window.cut_file) === 'undefined')
			window.cut_file = {};

		if (typeof (id) === 'undefined')
			window.cut_file = {};
		else if (id instanceof Array)
			$.each(id, function (i, v) {
				if (!v.isdir) window.cut_file[v.id] = v.name;
			});
		else if (id instanceof Object)
			window.cut_file = $.extend(window.cut_file, id);

		var len = Object.keys(window.cut_file).length;
		if (len > 0)
			$('.toolbar-admin [lay-event="paste"]').removeClass('layui-hide').html('<i class="layui-icon layui-icon-release"></i><span class="layui-badge">{0}</span>'.format(len));
		else
			$('.toolbar-admin [lay-event="paste"]').addClass('layui-hide').html('<i class="layui-icon layui-icon-release"></i>');
	}

	$('.nav-upload').on('click', function (e) {
		if (!layerLogin()) return;
		var index = layer.open({
			title: '文件上传(中转上传,速度较慢)',
			type: 1,
			maxmin: true,
			skin: 'layer-upload',
			content:
				'  <div class="layui-upload-drag">' +
				'      <i class="layui-icon"></i>' +
				'      <p>点击上传，或将文件拖拽到此处</p>' +
				'  </div>' +
				'<div class="layui-upload-list">' +
				'	<table class="layui-table">' +
				'		<colgroup>' +
				'			<col>' +
				'			<col width="80">' +
				'			<col width="150">' +
				'			<col width="114">' +
				'		</colgroup>' +
				'		<thead>' +
				'		<tr>' +
				'			<th>文件名</th>' +
				'			<th>大小</th>' +
				'			<th>进度</th>' +
				'			<th>操作</th>' +
				'		</tr>' +
				'		</thead>' +
				'		<tbody></tbody>' +
				'	</table>' +
				'</div>' +
				'<div class="layui-upload-btn">' +
				'	<button type="button" class="layui-btn upload-start">上传</button>' +
				'	<button type="button" class="layui-btn layui-btn-danger upload-close" style="float: right">关闭</button>' +
				'</div>'
			,
			success: function (elem, index) {
				$('.layer-upload > .layui-layer-content .upload-close').on('click', function () {
					layer.close(index);
				});
				var upList        = '.layer-upload > .layui-layer-content > .layui-upload-list > table > tbody',
					uploadListIns = upload.render({
						elem: '.layer-upload > .layui-layer-content > .layui-upload-drag',
						url: 'api.php?c=upload',
						data: {id: conf.id},
						accept: 'file',
						multiple: true,
						drag: true,
						number: 20,
						size: 102400,
						auto: false,
						bindAction: '.layer-upload > .layui-layer-content .upload-start',
						choose: function (obj) {
							var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
							//读取本地文件
							obj.preview(function (index, file, result) {
								var tr = $([
									'<tr id="upload-{0}">'.format(index),
									'<td>{0}</td>'.format(file.name),
									'<td>{0}</td>'.format(size_format(file.size)),
									'<td><div class="layui-progress" lay-filter="progress-demo-{0}"><div class="layui-progress-bar" lay-percent=""></div></div></td>'.format(index),
									'<td><button class="layui-btn layui-btn-xs upload-reload layui-hide">重传</button><button class="layui-btn layui-btn-xs layui-btn-danger upload-delete">删除</button></td>',
									'</tr>'
								].join(''));
								//单个重传
								tr.find('.upload-reload').on('click', function () {
									obj.upload(index, file);
								});

								//删除
								tr.find('.upload-delete').on('click', function () {
									delete files[index]; //删除对应的文件
									tr.remove();
									uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
									$(window).resize();
								});

								$(upList).append(tr);
								element.render('progress'); //渲染新加的进度条组件
								$(window).resize();
							});
						},
						before: function (obj) {
							if (Object.keys(this.files).length <= 0) return false;
							obj.preview(function (index, file, result) {
								var tr  = $(upList).find('tr#upload-' + index),
									tds = tr.children();
								tds.eq(2).html('<div class="layui-progress" lay-filter="progress-demo-{0}"><div class="layui-progress-bar" lay-percent=""></div></div>'.format(index));
								tds.eq(3).find('.upload-reload')
									.addClass('layui-disabled')
									.prop('disabled', true);
							});
							var elem = $('.layer-upload > .layui-layer-content .upload-start');
							if (elem.hasClass('layui-disabled')) return;
							elem.addClass('layui-disabled').prop('disabled', true)
								.prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
						},
						done: function (res, index, upload) { //成功的回调
							var tr  = $(upList).find('tr#upload-' + index),
								tds = tr.children();
							if (res.code === 0) { //上传成功
								tds.eq(2).html('<span class="layui-badge layui-bg-green">上传成功</span>');
								tds.eq(3).find('.upload-reload').addClass('layui-hide');
								delete this.files[index]; //删除文件队列已经上传成功的文件
							} else {
								tds.eq(2).html('<span class="layui-badge">{0}</span>'.format(res.msg));
								tds.eq(3).find('.upload-reload').removeClass('layui-hide')
									.removeClass('layui-disabled')
									.prop('disabled', false);
							}
						},
						allDone: function (obj) { //多文件上传完毕后的状态回调
							$('.layer-upload > .layui-layer-content .upload-start')
								.removeClass('layui-disabled').prop('disabled', false)
								.children('.layui-icon-loading').remove();
						},
						error: function (index, upload) { //错误回调
							var tr  = $(upList).find('tr#upload-' + index),
								tds = tr.children();
							tds.eq(3).find('.upload-reload').removeClass('layui-hide')
								.removeClass('layui-disabled')
								.prop('disabled', false); //显示重传
						},
						progress: function (n, elem, e, index) { //注意：index 参数为 layui 2.6.6 新增
							if (n >= 100) {
								var tr  = $(upList).find('tr#upload-' + index),
									tds = tr.children();
								tds.eq(2).html('<span class="layui-badge layui-bg-blue">服务器上传...</span>');
							} else
								element.progress('progress-demo-' + index, n + '%'); //执行进度条。n 即为返回的进度百分比
						}
					});
			}
		});
	});

	$('.nav-admin').on('click', function (e) {
		if (layerLogin())
			return layer.confirm('您已登录，现在退出管理员登录 ？', {
				title: '退出登录',
				shadeClose: true,
				btn: ['退出', '取消'],
				btn1: function (index) {
					lianyi.request({
						url: 'api.php?c=logout',
						msg: '退出登录...',
						success: function (res) {
							layer.close(index);
							window.conf.admin = false;
						}
					});
					return false;
				}
			});
	});

	new ClipboardJS('[copy-text]', {
		text: function (elem) {
			var tagName = $(elem).prop('tagName');
			if (tagName === 'INPUT' || tagName === 'TEXTAREA') return $(elem).val();
			var text = $(elem).attr('copy-text');
			if (!isEmpty(text)) return text;
			return $(elem).text();
		}
	}).on('success', function (e) {
		layer.msg($(e.trigger).text() + '成功', {shadeClose: true, time: 1500});
	}).on('error', function (e) {
		console.log(e);
	});

	new ClipboardJS('.copy-link', {
		text: function (elem) {
			var checkStatus = table.checkStatus('list'),
				checkeds    = checkStatus.data,
				urls        = [];

			$.each(checkeds, function (i, v) {
				if (!v.isdir) urls.push("{0}\t{1}".format(v.name, buildUrl(v.id + '/' + v.name)));
			});

			if (urls.length === 0) {
				lianyi.fail('无任何文件');
				return '';
			}

			return urls.join("\n");
		}
	}).on('success', function (e) {
		var len = e.text.split('\n').length;
		layer.msg('成功复制 <b class="layui-font-red">{0}</b> 个直链'.format(len), {shadeClose: true, time: 2000});
	}).on('error', function (e) {
		console.log(e);
	});

	$(window).on('hashchange', function (e) {
		parseHash();
		refresh();
	}).trigger('hashchange');
});